# __            __         __
#|  |_.--.--.--|  |_.--.--|  |_
#|   _|  |  |  |   _|_   _|   _|
#|____|________|____|__.__|____|
#
# twtxt is an open, distributed
# microblogging platform that
# uses human-readable text files,
# common transport protocols, and
# free software.
#
# Learn more about twtxt at https://github.com/buckket/twtxt
#
# public getwtxt instance available at:
#   https://twtxt.envs.net
#
# == Bio ==
#
# Builder and Maintainer of https://envs.net
#
# my website: https://envs.net/~creme/
# my projects: https://git.envs.net/creme
#
# == Metadata ==
#
# nick = creme
# url = https://envs.net/~creme/twtxt.txt
#
# == Content ==
#
2019-08-04T13:40:11+00:00	hello everyone, yeeah i write my first twtxt tweet.
2020-01-08T16:17:10+01:00	a new Matrix Instance now on envs.net! check it out: https://matrix.envs.net - https://envs.net/chat/matrix/
2020-01-22T13:34:09+01:00	a new mircoblogging Instance now on envs.net! check it out Pleroma - https://pleroma.envs.net
2020-03-13T13:30:20+01:00	hello @<ekkie https://envs.net/~ekkie/twtxt.txt> =)
2020-04-13T12:10:56+02:00	our getwtxt (registry server on https://twtxt.envs.net) is now on Version 0.4.13.
2020-05-20T08:18:05+02:00	i test today the new drone server on envs. ;) ( https://drone.envs.net/ )
